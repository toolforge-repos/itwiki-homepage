# Homepage of Tool:itwiki

This is the source code of this website:

https://itwiki.toolforge.org/

The website is designed to show some public log files to end-users and probably no much else.

## Update in production

To reflect your update in production you need access in the Tool:itwiki.

https://wikitech.wikimedia.org/wiki/Tool:Itwiki

Then just run this command from your computer:

```
ssh login.toolforge.org <<EOF
  become itwiki
  cd public_html
  git pull
EOF
```
