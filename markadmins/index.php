<?php
# Copyright (C) 2018 Valerio Bozzolan
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

require '../functions.php';

header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>MarkAdmins log</title>
	<meta name="robots" value="noindex" />
	<style>
	textarea {
		width: 90%;
		min-height:300px;
	}
	</style>
</head>
<body>
	<h1>MarkAdmins log</h1>
	<p>In seguito le ultime 200 righe di log di output dal bot che mantiene aggiornata la pagina <a href="https://it.wikipedia.org/wiki/Utente:ItwikiBot/AdminList">Utente:ItwikiBot/AdminList</a> che gira in questo server <a href="/">Wikimedia Toolforge</a> nel gruppo <a href="../">itwiki</a>.</p>
	<textarea><?php echo
		"[...]\n" .
		htmlentities( tail( '/data/project/itwiki/itwiki-markadmins.out', 200 ) )
	?></textarea>
	<h2>Collegamenti esterni</h2>
	<ul>
		<li><a href="https://it.wikipedia.org/wiki/Utente:Valerio_Bozzolan">Manutentore: Valerio B.</a></li>
		<li><a href="https://gitpull.it/source/ItalianWikipediaMarkAdmin/">Repository</a></li>
	</ul>
</body>
</html>
